# coding: utf-8
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
Pod::Spec.new do |s|
  s.name                   = "RCTRequired"
  s.version                = "0.61.5"
  s.summary                = "-"  # TODO
  s.homepage               = "http://facebook.github.io/react-native/"
  s.license                = "MIT"
  s.author                 = "Facebook, Inc. and its affiliates"
  s.platforms              = { :ios => "9.0", :tvos => "9.2" }
  s.source                 = { :git => "https://github.com/facebook/react-native.git", :tag => "v0.61.5" }
  s.source_files           = "Libraries/RCTRequired/**/*.{c,h,m,mm,cpp}"
  s.header_dir             = "RCTRequired"
end
