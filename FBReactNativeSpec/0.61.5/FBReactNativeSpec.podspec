# coding: utf-8
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

folly_compiler_flags = '-DFOLLY_NO_CONFIG -DFOLLY_MOBILE=1 -DFOLLY_USE_LIBCPP=1 -Wno-comma -Wno-shorten-64-to-32'
folly_version = '2018.10.22.00'

Pod::Spec.new do |s|
  s.name                   = "FBReactNativeSpec"
  s.version                = "0.61.5"
  s.summary                = "-"  # TODO
  s.homepage               = "http://facebook.github.io/react-native/"
  s.license                = "MIT"
  s.author                 = "Facebook, Inc. and its affiliates"
  s.platforms              = { :ios => "9.0", :tvos => "9.2" }
  s.compiler_flags         = folly_compiler_flags + ' -Wno-nullability-completeness'
  s.source                 = { :git => 'https://github.com/facebook/react-native.git', :tag => "v0.61.5" }
  s.source_files           = "Libraries/FBReactNativeSpec/**/*.{c,h,m,mm,cpp}"
  s.header_dir             = "FBReactNativeSpec"

  s.pod_target_xcconfig    = {
                               "USE_HEADERMAP" => "YES",
                               "CLANG_CXX_LANGUAGE_STANDARD" => "c++14",
                               "HEADER_SEARCH_PATHS" => "\"$(PODS_TARGET_SRCROOT)/Libraries/FBReactNativeSpec\" \"$(PODS_ROOT)/Folly\""
                             }

  s.dependency "Folly", folly_version
  s.dependency "RCTRequired", "0.61.5"
  s.dependency "RCTTypeSafety", "0.61.5"
  s.dependency "React-Core", "0.61.5"
  s.dependency "React-jsi", "0.61.5"
  s.dependency "ReactCommon/turbomodule/core", "0.61.5"
end
